/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.octest.beans;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class gererbdd {
    
    private Connection con = null;
    
    
    public List<client> recupereClient(){ 
    
    List<client> clien= new  ArrayList<>();
    
 
          Statement st = null;
          ResultSet rst = null;
          
          // connexion à la base de données
          loadDataBase();
   
        try{
        
            st= con.createStatement();
            
           String query;
            query= "select * from client11";
            rst=st.executeQuery(query);
            
            // recupere les donner
            while(rst.next()){
                int id= rst.getInt("Id");
                String nom= rst.getString("Nom");
                String prenom= rst.getString("Prenom");
                String Adresse= rst.getString("Adresse");
                
                client utilisateur= new client();
                
                utilisateur.setId(id);
                utilisateur.setNom(nom);
                utilisateur.setPrenom(prenom);
                utilisateur.setAdresse(Adresse);
                
                
                clien.add(utilisateur);
                
            }
            
            
        } catch(SQLException e){
            
        }finally{
            // fermeture de la connection
            try{
                if(rst != null)
                    rst.close();
                if( st != null)
                    st.close();
                if(con != null)
                    con.close();
                
            }catch(SQLException ignore){
                
            }
        }
   
    return clien;
    
    
  } // fin d'une fonction
    
    
    
    
    
    
        
    public void ajouter_client(client client11){
        
        loadDataBase();
        
        try{
            
            PreparedStatement prepareStatement = con.prepareStatement("INSERT INTO client11(Id,Nom,Prenom,Adresse) VALUES(?,?,?,?);");
            prepareStatement.setInt(1, client11.getId());
            prepareStatement.setString(2, client11.getNom());
            prepareStatement.setString(3, client11.getPrenom());
            prepareStatement.setString(4, client11.getAdresse());
            
            prepareStatement.executeUpdate();

        
        }catch(SQLException e){
            e.printStackTrace();
        }
        
    }
    
    //////////////////////////////////////////////////
    
        public List<client> recherche_client_id(int identifiant){ 
    
    List<client> clien= new  ArrayList<>();
    
 
          Statement st = null;
          ResultSet rst = null;
          
          // connexion à la base de données
          loadDataBase();
   
        try{
        
            st= con.createStatement();
            
           String query;
            query= "select * from client11 where Id="+identifiant+"  ";
            rst=st.executeQuery(query);
            
            // recupere les donner
            while(rst.next()){
                int id= rst.getInt("Id");
                String nom= rst.getString("Nom");
                String prenom= rst.getString("Prenom");
                String Adresse= rst.getString("Adresse");
                
                client utilisateur= new client();
                
                utilisateur.setId(id);
                utilisateur.setNom(nom);
                utilisateur.setPrenom(prenom);
                utilisateur.setAdresse(Adresse);
                
                
                clien.add(utilisateur);
                
            }
            
            
        } catch(SQLException e){
            
        }finally{
            // fermeture de la connection
            try{
                if(rst != null)
                    rst.close();
                if( st != null)
                    st.close();
                if(con != null)
                    con.close();
                
            }catch(SQLException ignore){
                
            }
        }
   
    return clien;
    
    
    
  } // fin d'une fonction
    
        
        
        
        
        
        
         public List<client> recherche_client_name(String name2){ 
    
    List<client> clien= new  ArrayList<>();
    
 
          Statement st = null;
          ResultSet rst = null;
          
          // connexion à la base de données
          loadDataBase();
   
        try{
        
            st= con.createStatement();
            
           String query;
            query= "select Id,Nom,Prenom,Adresse from client11 where  Nom= '"+name2+"' ";
            rst=st.executeQuery(query);
            
            // recupere les donner
            while(rst.next()){
                int id= rst.getInt("Id");
                String nom= rst.getString("Nom");
                String prenom= rst.getString("Prenom");
                String Adresse= rst.getString("Adresse");
                
                client utilisateur= new client();
                
                utilisateur.setId(id);
                utilisateur.setNom(nom);
                utilisateur.setPrenom(prenom);
                utilisateur.setAdresse(Adresse);
                
                
                clien.add(utilisateur);
                
            }
            
            
        } catch(SQLException e){
            
        }finally{
            // fermeture de la connection
            try{
                if(rst != null)
                    rst.close();
                if( st != null)
                    st.close();
                if(con != null)
                    con.close();
                
            }catch(SQLException ignore){
                
            }
        }
   
    return clien;
    
    
  } // fin d'une fonction
    /////////////////////////////////////////////////
         
         
         
         
        
        public void suppression_client(int identifiant){
            
          Statement st = null;
          ResultSet rst = null;
          
          // connexion à la base de données
          loadDataBase();
   
        try{
           st= con.createStatement();
            
           String query;
            query= "delete from client11 where Id="+identifiant+"  ";
            st.executeUpdate(query);
            
        } catch(SQLException e){
            
        }finally{
            // fermeture de la connection
            try{
                if(rst != null)
                    rst.close();
                if( st != null)
                    st.close();
                if(con != null)
                    con.close();
                
            }catch(SQLException ignore){
                
            }
        }
  } // fin d'une fonction     
         
         
         
         
        
        
    /////////////////////////////////////////////////
    
    
   // la connection dans une base de données
   public void loadDataBase(){
       
       try{
         Class.forName("com.mysql.jdbc.Driver");
   }catch(ClassNotFoundException e){
       
   }
       try{
           con= DriverManager.getConnection("jdbc:mysql://localhost:3306/base_ide","root","");
       }catch(SQLException e){
           e.printStackTrace();
       }
   }/////////////fin fonction
 
}
