/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.octest.servelets;

import com.octest.beans.ConnectionForm;
import com.octest.beans.ajouter_personne;
import com.octest.beans.client;
import com.octest.beans.gererbdd;
import com.octest.beans.modification_model;
import java.io.IOException;
import java.sql.SQLException;
import java.sql.Types;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ali
 */
public class accueil extends HttpServlet {

    
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        
        gererbdd gerer= new gererbdd();
        request.setAttribute("client", gerer.recupereClient());
 
       this.getServletContext().getRequestDispatcher("/WEB-INF/accueil.jsp").forward(request, response);
    }

   
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        // on recupere l'identifiant 
        
        boolean success = false; 
        boolean echec = false;
        
        gererbdd bd= new gererbdd();
        try{
        int tempo = Integer.parseInt(request.getParameter("id"));
        request.setAttribute("tempo", tempo);
        request.setAttribute("nn", bd.recherche_client_id(tempo));
        }catch(Exception e){
            
        }
         ////////////////////////////////////////// fin recupere identifiant

        client cl = new client();
        gererbdd gerer= new gererbdd();

        String n = request.getParameter("nom");
        String p = request.getParameter("prenom");
        String a = request.getParameter("adress");
        
       
  
        
        
        if( n != null &&  p!=null && a != null){
        try{
            
             cl.setId(Integer.parseInt(request.getParameter("id")));
        }catch(Exception e){
            
        }
        cl.setNom(request.getParameter("nom"));
        cl.setPrenom(request.getParameter("prenom"));
        cl.setAdresse(request.getParameter("adress"));
        
         /// on va gerer le message d'ajout succès de client
        if(! p.isEmpty() && ! n.isEmpty() && ! a.isEmpty() && ! p.contains(" ") && ! n.contains(" ") && ! a.contains("  ")){
        success = true;
        request.setAttribute("success", success);
        }
        
        /// on va gerer le message d'échec d'ajout client
        if( p.isEmpty() ||  n.isEmpty() || a.isEmpty() || p.contains(" ") || n.contains(" ") || a.contains("  ")){
        echec = true;
        request.setAttribute("echec", echec);
        }
        
        
        // ajouter un client dans la base de donnée
        
        
        if(! p.isEmpty() && ! n.isEmpty() && ! a.isEmpty() && ! p.contains(" ") && ! n.contains(" ") && ! a.contains("  ")){
        gerer.ajouter_client(cl);
        
        request.setAttribute("client", gerer.recupereClient()); 
        
        
        
        this.getServletContext().getRequestDispatcher("/WEB-INF/accueil.jsp").forward(request, response);
        
        }
        }
        
        
        ///// La supression d'avant
        /*
        try{
            
        int suppression = Integer.parseInt(request.getParameter("supp"));
        gerer.suppression_client(suppression);
        request.setAttribute("a", suppression);
        }catch(Exception e){
            
        }
        
        */

        ////// supression direct dans la base de donnée
         try{
            
        int su = Integer.parseInt(request.getParameter("suprim"));
        gerer.suppression_client(su);
        request.setAttribute("ss", su);
        }catch(Exception e){
            
        }
         
        

        /////////////////////////////////////////////////////////////////
        try{
        // recherche un client dans une base de données avec id
        
        int tempo= Integer.parseInt(request.getParameter("idi"));
        request.setAttribute("recherche", gerer.recherche_client_id(tempo));

        }catch(Exception e){
            
        }
        
        //// recherche un client dans une base de données avec le name
        String tempo1= request.getParameter("name1");
        request.setAttribute("name8", gerer.recherche_client_name(tempo1));
        
        // afficher liste client
        request.setAttribute("client", gerer.recupereClient());
        
        this.getServletContext().getRequestDispatcher("/WEB-INF/accueil.jsp").forward(request, response);
        

    }

    
    
    
    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
