package main;

import java.io.BufferedReader;
import java.sql.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;



public class connectionavecDB {
    
    
    private Connection con;
    private Statement st;
    private ResultSet rst;
    
    // connection dans la base de donnée
    public connectionavecDB(){
        try{
            Class.forName("com.mysql.jdbc.Driver");
            //Class.forName("org.gjt.mm.mysql.Driver");
            con= DriverManager.getConnection("jdbc:mysql://localhost:3306/base_ide","root","");
            st= con.createStatement();
            System.out.println("la connection a réuissi avec succès");
        } catch(Exception ex){
            System.out.println("Error: " + ex);
        }
    }
    
    
    // lire dans une base de donnée
    public void getData(){
        try{
            String query;
            query= "select * from client2";
            rst=st.executeQuery(query);
            while(rst.next()){
                int id= rst.getInt("Id");
                String nom= rst.getString("Nom");
                String prenom= rst.getString("Prenom");
                String adresse= rst.getString("Adresse");
                System.out.println("l'id est: "+ id+ " le nom est: "+ nom+ " le Prenom est: "+ prenom +" l'adresse est: "+ adresse);
            }
            
            
        }catch(Exception ex){
            System.out.println(ex);
        }
    }
    
    
    
    // lire dans un fichier
    public void lire_fichier(){
        
        File file= new File("/home/ali/Bureau/utilisation-des-ide/client.txt");
        
        if(! file.exists()){
            System.out.println("Erreur le fichier n'exite pas on ne vas pas le traiter");
        }
        
        else{
            
            try{
            BufferedReader reader= new BufferedReader(new InputStreamReader(new FileInputStream(file),"UTF-8"));
        
            String line = reader.readLine();
            
            /*String rr = " "+line+" ";
            String[] str= line.split(": ");
            System.out.println(str[1]);*/
           int compteur=0;
            
            while(line != null){
               
                if(compteur == 4){ System.out.println(compteur); compteur=0;}
                if(line.contains("id: ") || line.contains("nom: ") || line.contains("prenom: ") || line.contains("adresse: ") ){
                  String[] str1= line.split(": "); 
                  System.out.println(str1[1]);  
                  compteur++;
                }
                  
                line= reader.readLine(); // pour passer à la ligne
            }
            
            reader.close();
            
            }catch(IOException e){
                e.printStackTrace();
            }      
                    
        }
    }
    
    
    
    
    
    
    // lire dans un fichier et faire l'ajout dans une base de données
    public void lire_fichier2(){
        
        File file= new File("/home/ali/Bureau/utilisation-des-ide/client.txt");
        
        if(! file.exists()){
            System.out.println("Erreur le fichier n'exite pas on ne va pas le traiter");
        }
        
        else{
            
            try{
            BufferedReader reader= new BufferedReader(new InputStreamReader(new FileInputStream(file),"UTF-8"));
        
            String line = reader.readLine();
            
            /*String rr = " "+line+" ";
            String[] str= line.split(": ");
            System.out.println(str[1]);*/
            
            
            
           boolean boolenne = true; // Pour stopper la lecture du fichier en cas d'érreur
           ArrayList<String> tempo=new ArrayList<>();
           ArrayList<String> donnees=new ArrayList<>();
           
           // si le fichier est vide c'est à dire que la premiere ligne est vide
           if(line == null) System.out.println("Error le fichier est vide on n'a pas de donnée à traiter");
            
           //si c'est pas vide
            while(line != null && boolenne){
               if(line.contains("id: ")){
                for(int i=0; i<4;i++){
                  if(line.contains("id: ") || line.contains("nom: ") || line.contains("prenom: ") || line.contains("adresse: ") ){
                  String[] str1= line.split(": ");
                  System.out.println(str1[1]);
                  tempo.add(str1[0]);
                  donnees.add(str1[1]);
                  line= reader.readLine(); // pour passer à la ligne
                }
                  
                }
                
                 if(tempo.contains("id") && tempo.contains("nom") && tempo.contains("prenom") && tempo.contains("adresse") )
                 {
                     System.out.println(" niquel certaines données ont été lu avec succès et vont etre rajouté dans la base de donnée");
                     /////////////////////////////###########################################""///////////////////////////////////////////////
                   
                    try{
          
                           String query;
                           query= "INSERT INTO base_ide.client2 (Id, Nom, Prenom, Adresse) VALUES (' "+donnees.get(0)+" ' , ' "+donnees.get(1)+" ' , ' "+donnees.get(2)+" ' , ' "+donnees.get(3)+" ' );";
                           st.execute(query);
             
                        }catch(Exception ex)
                          {
                            System.out.println(ex);
                          }
                   
                   
                      /////////////////////////////###########################################///////////////////////////////////////////////
                  
                      // néttoyage des 4 données
                      tempo.clear();
                      donnees.clear();
                } 
                 
                 else
                 {
                   System.out.println("erreur le fichier est invalide et ne peut pas etre traiter");
                      // néttoyage des 4 données
                      tempo.clear();
                      donnees.clear();
                      boolenne=false;
                   
                 }
                
                
               }
                
              
                  line = reader.readLine();
                
            }
            
            reader.close();
            
            }catch(IOException e){
                e.printStackTrace();
            }      
                    
        }
    }
    
    
    
    
    
    // l'ajout dans une base de donnée
    public void setData(){
        
         try{
          
             String alpha2="mister";
             
             String query;
             query= "INSERT INTO base_ide.client2 (Id, Nom, Prenom, Adresse) VALUES (6,' "+alpha2+" ' ,'ali5','976 mayotte');";
             st.execute(query);
             
        }catch(Exception ex){
            System.out.println(ex);
        }
        
    }
    
    
    
    
    
    
}


